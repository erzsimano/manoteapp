package hu.bme.manoteapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

public class Curve implements Serializable
{
	private List<SerializablePoint> points;
	private SerializableColor color;

	public Curve()
	{
		points = new ArrayList<SerializablePoint>();
		this.color = new SerializableColor(Color.BLACK);
	}

	// Add a point given with coordinates
	public void addPoint(double x, double y)
	{
		points.add(new SerializablePoint(x, y));
	}

	// Majd ha ki akarod menteni a pontokat, j� lesz elk�rni
	public List<SerializablePoint> getPoints()
	{
		return points;
	}

	// J�l j�n, ha pl pontokat akarsz visszavenni f�jlb�l egy list�ban, nem
	// pedig egyes�vel add-olgani a pontokat
	public void setPoints(List<SerializablePoint> points)
	{
		this.points = points;
	}

	public void setColor(int color)
	{
		this.color.set(color);
	}

	public SerializableColor getColor()
	{
		return color;
	}

	protected class SerializableColor implements Serializable
	{
		private static final long serialVersionUID = 1248005110892499338L;
		int r, g, b;

		public SerializableColor(int color)
		{
			r = Color.red(color);
			g = Color.green(color);
			b = Color.blue(color);
		}

		public SerializableColor(int r, int g, int b)
		{
			this.r = r;
			this.g = g;
			this.b = b;
		}

		public void set(int color)
		{
			r = Color.red(color);
			g = Color.green(color);
			b = Color.blue(color);
		}
	}

	protected class SerializablePoint implements Serializable
	{
		private static final long serialVersionUID = 6876762728893078134L;
		private double x, y;

		public SerializablePoint(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public double getX()
		{
			return x;
		}

		public void setX(double x)
		{
			this.x = x;
		}

		public double getY()
		{
			return y;
		}

		public void setY(double y)
		{
			this.y = y;
		}

	}
}
