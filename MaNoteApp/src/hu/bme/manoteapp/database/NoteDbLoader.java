package hu.bme.manoteapp.database;

import hu.bme.manoteapp.NoteFront;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NoteDbLoader
{
	private Context ctx;
	private NoteDbHelper dbHelper;
	private SQLiteDatabase mDb;

	public NoteDbLoader(Context ctx)
	{
		this.ctx = ctx;
	}

	public void open() throws SQLException
	{
		dbHelper = new NoteDbHelper(ctx, NoteTableConstants.DATABASE_NAME);
		mDb = dbHelper.getWritableDatabase();
		dbHelper.onCreate(mDb);
	}

	public void close()
	{
		dbHelper.close();
	}

	// INSERT
	public long createNote(NoteFront nft)
	{
		ContentValues values = new ContentValues();
		values.put(NoteTableConstants.NoteFront.COLUMN_ID, nft.getId());
		values.put(NoteTableConstants.NoteFront.COLUMN_TITLE, nft.getTitle());
		values.put(NoteTableConstants.NoteFront.COLUMN_ICONTYPE, nft.getIconType());
		values.put(NoteTableConstants.NoteFront.COLUMN_FILEDIR, nft.getFileDir());
		values.put(NoteTableConstants.NoteFront.COLUMN_DRIVE_ID, nft.getDriveId());

		return mDb.insert(NoteTableConstants.NoteFront.DATABASE_TABLE, null, values);
	}

	// DELETE
	public boolean deleteNote(NoteFront nft)
	{
		mDb.delete(NoteTableConstants.NoteFront.DATABASE_TABLE, NoteTableConstants.NoteFront.COLUMN_ID + "=" + nft.getId(), null);
		return true;
	}

	// UPDATE
	public boolean updateNote(long id, NoteFront nft)
	{
		ContentValues values = new ContentValues();
		values.put(NoteTableConstants.NoteFront.COLUMN_ID, nft.getId());
		values.put(NoteTableConstants.NoteFront.COLUMN_TITLE, nft.getTitle());
		values.put(NoteTableConstants.NoteFront.COLUMN_ICONTYPE, nft.getIconType());
		values.put(NoteTableConstants.NoteFront.COLUMN_FILEDIR, nft.getFileDir());
		values.put(NoteTableConstants.NoteFront.COLUMN_DRIVE_ID, nft.getDriveId());

		if(NoteTableConstants.NoteFront.COLUMN_ID == Long.toString(nft.getId()))
		{
			mDb.update(NoteTableConstants.NoteFront.DATABASE_TABLE, values, NoteTableConstants.NoteFront.COLUMN_ID + "=" + nft.getId(), null);
			return true;
		} else
			return false;
	}

	public void deleteAll()
	{
		mDb.execSQL(NoteTableConstants.DATABASE_DROP_ALL);
		mDb.execSQL(NoteTableConstants.DATABASE_CREATE_ALL);
	}

	public Cursor fetchAll()
	{
		return mDb.query(NoteTableConstants.NoteFront.DATABASE_TABLE, new String[] { NoteTableConstants.NoteFront.COLUMN_ID,
				NoteTableConstants.NoteFront.COLUMN_TITLE, NoteTableConstants.NoteFront.COLUMN_ICONTYPE, NoteTableConstants.NoteFront.COLUMN_FILEDIR,
				NoteTableConstants.NoteFront.COLUMN_DRIVE_ID }, null, null, null, null, NoteTableConstants.NoteFront.COLUMN_TITLE);
	}

	public NoteFront fetchNoteFront(long rowId)
	{
		Cursor c = mDb.query(NoteTableConstants.NoteFront.DATABASE_TABLE, new String[] { NoteTableConstants.NoteFront.COLUMN_ID,
				NoteTableConstants.NoteFront.COLUMN_TITLE, NoteTableConstants.NoteFront.COLUMN_ICONTYPE, NoteTableConstants.NoteFront.COLUMN_FILEDIR,
				NoteTableConstants.NoteFront.COLUMN_DRIVE_ID }, null, null, null, null, NoteTableConstants.NoteFront.COLUMN_TITLE);
		if(c.moveToFirst())
			return getNoteFrontByCursor(c);
		return null;
	}

	public static NoteFront getNoteFrontByCursor(Cursor c)
	{
		return new NoteFront(c.getInt(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ID)), // id
				c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_TITLE)), // title
				c.getInt(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ICONTYPE)), // icontype
				c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_FILEDIR)), // filedir
				c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_DRIVE_ID)));
	}
}
