package hu.bme.manoteapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class NoteDbHelper extends SQLiteOpenHelper
	{
		public NoteDbHelper(Context context, String name)
	{
		super(context, name, null, NoteTableConstants.DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

		@Override
		public void onCreate(SQLiteDatabase db)
		{
			db.execSQL(NoteTableConstants.DATABASE_CREATE_ALL);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		{
			db.execSQL(NoteTableConstants.DATABASE_DROP_ALL);
			db.execSQL(NoteTableConstants.DATABASE_CREATE_ALL);
		}
}
