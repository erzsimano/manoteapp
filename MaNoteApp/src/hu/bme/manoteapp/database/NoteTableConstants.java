package hu.bme.manoteapp.database;

public class NoteTableConstants
{
	public static final String DATABASE_NAME = "data.db";
	// verzioszam
	public static final int DATABASE_VERSION = 1;
	// osszes belso osztaly DATABASE_CREATE szkriptje osszefuzve
	public static String DATABASE_CREATE_ALL = NoteFront.DATABASE_CREATE;
	// osszes belso osztaly DATABASE_DROP szkriptje osszefuzve
	public static String DATABASE_DROP_ALL = NoteFront.DATABASE_DROP;

	public static class NoteFront
	{
		public static final String DATABASE_TABLE = "note";
		public static final String COLUMN_ID = "_id";
		public static final String COLUMN_ICONTYPE = "icontype";
		public static final String COLUMN_TITLE = "title";
		public static final String COLUMN_FILEDIR = "filedir";
		public static final String COLUMN_DRIVE_ID = "driveid";

		private static final String DATABASE_CREATE = "create table if not exists " + DATABASE_TABLE + " ( " + COLUMN_ID + " integer primary key, "
				+ COLUMN_TITLE + " text not null, " + COLUMN_ICONTYPE + " integer, " + COLUMN_FILEDIR + " text, " + COLUMN_DRIVE_ID + " text"
				+ " ); ";

		public static final String DATABASE_DROP = "drop table if exists " + DATABASE_TABLE + "; ";
	}
}
