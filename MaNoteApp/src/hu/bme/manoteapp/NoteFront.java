package hu.bme.manoteapp;

import hu.bme.manoteapp.database.NoteTableConstants;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

public class NoteFront implements Parcelable
{
	public static enum IconType {
		yellow, red, white, green, blue, grey, purple
	};

	private long id;
	private IconType iconType;
	private String title;
	private String fileDir;
	private String driveId;

	public NoteFront()
	{

	}
	
	public NoteFront(Cursor c)
	{
		id = c.getLong(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ID));
		title = c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_TITLE));
		int icon = c.getInt(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ICONTYPE));
		iconType = resolveIntToIconType(icon);
		fileDir = c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_FILEDIR));
		driveId = c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_DRIVE_ID));
	}

	public NoteFront(long id, String t, int icon, String fileDir, String driveId)
	{
		iconType = resolveIntToIconType(icon);
		this.id = id;
		setTitle(t);
		setFileDir(fileDir);
		setDriveId(driveId);
	}

	public NoteFront(Parcel in)
	{
		this.id = in.readLong();
		this.title = in.readString();

		int icon;
		icon = in.readInt();

		iconType = resolveIntToIconType(icon);

		this.fileDir = in.readString();

		this.driveId = in.readString();
	}

	public long getId()
	{
		return id;
	}

	public int getIconType()
	{
		switch (iconType)
		{
			case yellow:
				return 0;
			case red:
				return 1;
			case white:
				return 2;
			case green:
				return 3;
			case blue:
				return 4;
			case grey:
				return 5;
			case purple:
				return 6;
			default:
				return 0;
		}

	}

	public int resolveIconTypeToInt(IconType iconType)
	{
		switch (iconType)
		{
			case yellow:
				return 0;
			case red:
				return 1;
			case white:
				return 2;
			case green:
				return 3;
			case blue:
				return 4;
			case grey:
				return 5;
			case purple:
				return 6;
			default:
				return 0;
		}
	}
	
	public IconType resolveIntToIconType(int i)
	{
		IconType iType = IconType.yellow;
		switch (i)
		{
			case 0:
				iType = IconType.yellow;
				break;
			case 1:
				iType = IconType.red;
				break;
			case 2:
				iType = IconType.white;
				break;
			case 3:
				iType = IconType.green;
				break;
			case 4:
				iType = IconType.blue;
				break;
			case 5:
				iType = IconType.grey;
				break;
			case 6:
				iType = IconType.purple;
				break;
		}
		return iType;
	}

	public void setIconType(IconType iconType)
	{
		this.iconType = iconType;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getFileDir()
	{
		return fileDir;
	}

	public void setFileDir(String fileDir)
	{
		this.fileDir = fileDir;
	}

	public String getDriveId()
	{
		return driveId;
	}

	public void setDriveId(String driveId)
	{
		this.driveId = driveId;
	}

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeLong(id);
		dest.writeString(title);
		dest.writeInt(resolveIconTypeToInt(iconType));
		dest.writeString(fileDir);
		dest.writeString(driveId);
	}

	public static final Parcelable.Creator<NoteFront> CREATOR = new Parcelable.Creator<NoteFront>()
	{
		@Override
		public NoteFront createFromParcel(Parcel in)
		{
			return new NoteFront(in);
		}

		@Override
		public NoteFront[] newArray(int size)
		{
			return new NoteFront[size];
		}
	};
}
