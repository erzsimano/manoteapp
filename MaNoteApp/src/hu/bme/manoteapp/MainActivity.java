package hu.bme.manoteapp;

import hu.bme.manoteapp.adapter.GridAdapter;
import hu.bme.manoteapp.database.NoteDbLoader;
import hu.bme.manoteapp.database.NoteTableConstants;
import hu.bme.manoteapp.sync.DriveActivity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;

public class MainActivity extends DriveActivity
{
	public static final String FILE_DIR = "manoteapp";

	private ArrayList<NoteFront> array;
	private GridView gridview;
	private GridAdapter adapter;
	private NoteDbLoader nDbLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getActionBar().show();

		if(nDbLoader == null)
			nDbLoader = new NoteDbLoader(this);
		else
			nDbLoader.close();

		nDbLoader.open();

		array = new ArrayList<NoteFront>();
		array.add(new NoteFront());
		Cursor data = nDbLoader.fetchAll();
		NoteFront nf;
		if(data.moveToFirst())
		{
			while(data.isAfterLast() == false)
			{
				long id = data.getLong(data.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ID));
				String title = data.getString(data.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_TITLE));
				int type = data.getInt(data.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_ICONTYPE));
				String dir = data.getString(data.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_FILEDIR));
				String driveId = data.getString(data.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_DRIVE_ID));
				nf = new NoteFront(id, title, type, dir, driveId);
				array.add(nf);
				data.moveToNext();
			}
		}
		data.close();

		gridview = (GridView) findViewById(R.id.grid_id);
		gridview.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id)
			{
				if(position == 0)
				{
					Intent intent_cr = new Intent(MainActivity.this, CreateNoteFrontActivity.class);
					startActivityForResult(intent_cr, 100);

				} else
				{
					Intent intent = new Intent(MainActivity.this, EditActivity.class);
					intent.putExtra("note_title", array.get(position).getTitle());
					startActivity(intent);
				}
			}
		});
		Log.d("GRID ERTEK", Integer.toString(gridview.getCount()));

		adapter = new GridAdapter(array);
		gridview.setAdapter(adapter);
		registerForContextMenu(gridview);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		if(v.equals(gridview))
		{
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
			menu.setHeaderTitle(((NoteFront) adapter.getItem(info.position)).getTitle());
			String[] menuItems = getResources().getStringArray(R.array.notemenu);
			for(int i = 0; i < menuItems.length; i++)
			{
				menu.add(Menu.NONE, i, i, menuItems[i]);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		int menuItemIndex = item.getItemId();
		final NoteFront selectedNote = (NoteFront) adapter.getItem(info.position);
		if(menuItemIndex == 0)
		{
			Intent intent = new Intent(MainActivity.this, EditActivity.class);
			intent.putExtra("note_title", selectedNote.getTitle());
			startActivity(intent);

		} else if(menuItemIndex == 1)
		{
			// DELETE

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			alertDialogBuilder.setTitle("Delete file");
			alertDialogBuilder.setMessage("Do you really want to delete this file?");
			alertDialogBuilder.setCancelable(false);

			alertDialogBuilder.setPositiveButton("Yes", new OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + MainActivity.FILE_DIR;
					File f = new File(dir + File.separator + selectedNote.getTitle() + ".mano");
					f.delete();
					nDbLoader.deleteNote(selectedNote);
					array.remove(info.position);
					adapter.notifyDataSetChanged();
				}
			});

			alertDialogBuilder.setNegativeButton("No", new OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.cancel();
				}
			});

			AlertDialog alertdialog = alertDialogBuilder.create();
			alertdialog.show();

		} else
		{
			// BACK
		}
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == 100)
		{
			Bundle b = data.getExtras();
			NoteFront newNote = b.getParcelable("hu.bme.manoteapp.NoteFront");
			try
			{
				new File(newNote.getFileDir()).createNewFile();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			array.add(newNote);
			nDbLoader.createNote(newNote);
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.exit:
				showAlertExit();
				break;
			// case R.id.connect_drive:
			// connectDrive();
			// break;
			case R.id.connect_drive_new_account:
				connectDriveWithNewAccount();
				break;
			case R.id.upload_drive:
				syncFilesToDrive();
				break;
		}
		return true;
	}
	
	public void syncFilesToDrive()
	{
		Cursor data = nDbLoader.fetchAll();
		if(data.moveToFirst())
		{
			while(data.isAfterLast() == false)
			{
				NoteFront nf = new NoteFront(data);
				if(nf.getDriveId() != null && nf.getDriveId().isEmpty() == false)
				{
					syncFile(nf);
				}
				else{
					uploadNewFileToDrive(nf, nDbLoader);
				}
				data.moveToNext();
			}
		}
		data.close();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		nDbLoader.close();
	}

	@Override
	public void onBackPressed()
	{
		showAlertExit();
	}

	public void showAlertExit()
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("Exit");
		alertDialogBuilder.setMessage("Do you really want to exit?");
		alertDialogBuilder.setCancelable(false);

		alertDialogBuilder.setPositiveButton("Yes", new OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				MainActivity.this.finish();
			}
		});

		alertDialogBuilder.setNegativeButton("No", new OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});

		AlertDialog alertdialog = alertDialogBuilder.create();
		alertdialog.show();
	}

	@Override
	public void showMessage(String text)
	{
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
	}
}
