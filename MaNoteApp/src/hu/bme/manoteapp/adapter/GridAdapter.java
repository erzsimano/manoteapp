package hu.bme.manoteapp.adapter;

import hu.bme.manoteapp.NoteFront;
import hu.bme.manoteapp.R;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter
{
	ArrayList<NoteFront> array;

	public GridAdapter(ArrayList<NoteFront> arr)
	{
		array = arr;
	}

	@Override
	public int getCount()
	{
		return array.size();
	}

	@Override
	public Object getItem(int arg0)
	{
		return array.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{

		LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View gridview = inflater.inflate(R.layout.grid, null);
		ImageView image = (ImageView) gridview.findViewById(R.id.addview);
		TextView textViewTitle = (TextView) gridview.findViewById(R.id.textadd);

		if (position == 0)
		{
			image.setImageResource(R.drawable.pinky);
			textViewTitle.setText(R.string.add);
		}
		else
		{
			switch(array.get(position).getIconType())
			{
			case 0:
				image.setImageResource(R.drawable.note_yellow);
				break;
			case 1:
				image.setImageResource(R.drawable.note_red);
				break;
			case 2:
				image.setImageResource(R.drawable.note_white);
				break;
			case 3:
				image.setImageResource(R.drawable.note_green);
				break;
			case 4:
				image.setImageResource(R.drawable.note_blue);
				break;
			case 5:
				image.setImageResource(R.drawable.note_grey);
				break;
			case 6:
				image.setImageResource(R.drawable.note_purple);
				break;
			default:
				image.setImageResource(R.drawable.note_yellow);
				break;
			}
			textViewTitle.setText(array.get(position).getTitle());
		}
		return gridview;
	}
}
