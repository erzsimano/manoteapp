package hu.bme.manoteapp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SpinnerAdapter extends BaseAdapter
{

	private ArrayList<String> array;

	public SpinnerAdapter(ArrayList<String> a)
	{
		array = a;
	}

	@Override
	public int getCount()
	{
		return array.size();
	}

	@Override
	public Object getItem(int position)
	{
		return array.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View root = inflater.inflate(android.R.layout.simple_spinner_item, null);
		TextView tv = (TextView)root.findViewById(android.R.id.text1);
		tv.setText(array.get(position));
		
		return root;
	}

}
