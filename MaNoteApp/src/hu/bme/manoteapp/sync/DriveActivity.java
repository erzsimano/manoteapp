package hu.bme.manoteapp.sync;

import hu.bme.manoteapp.NoteFront;
import hu.bme.manoteapp.database.NoteDbLoader;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.ContentsResult;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;

public class DriveActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
	private static final String TAG = "DriveActivity";

	/**
	 * Extra for account name.
	 */
	protected static final String EXTRA_ACCOUNT_NAME = "account_name";

	/**
	 * Request code for auto Google Play Services error resolution.
	 */
	protected static final int REQUEST_CODE_RESOLUTION = 1;
	/**
	 * Next available request code.
	 */
	protected static final int NEXT_AVAILABLE_REQUEST_CODE = 2;

	protected static final int DRIVE_ACCOUNT_RESELECT = 3;

	/**
	 * Google API client.
	 */
	protected GoogleApiClient mGoogleApiClient;

	private String account;

	private ArrayList<Metadata> mResults;
	private String mNextPageToken;
	private boolean mHasMore;

	private DriveId mMainFolderId;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mHasMore = true;
		mResults = new ArrayList<Metadata>();
		mGoogleApiClient = null;
		account = null;
		mMainFolderId = null;
	}

	/**
	 * Called when activity gets visible. A connection to Drive services need to
	 * be initiated as soon as the activity is visible. Registers
	 * {@code ConnectionCallbacks} and {@code OnConnectionFailedListener} on the
	 * activities itself.
	 */
	@Override
	protected void onResume()
	{
		super.onResume();
		connectDrive();
	}

	/**
	 * Called when activity gets invisible. Connection to Drive service needs to
	 * be disconnected as soon as an activity is invisible.
	 */
	@Override
	protected void onPause()
	{
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
		{
			mGoogleApiClient.disconnect();
		}
		super.onPause();
	}

	/**
	 * Handles resolution callbacks.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_RESOLUTION && resultCode == RESULT_OK)
		{
			mGoogleApiClient.connect();
		}

		if (requestCode == DRIVE_ACCOUNT_RESELECT && resultCode == RESULT_OK)
		{
			if (data != null)
			{
				mGoogleApiClient.disconnect();
				mGoogleApiClient = null;

				account = data.getExtras().getString(AccountManager.KEY_ACCOUNT_NAME);

				if (mGoogleApiClient == null)
				{
					mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API).addScope(Drive.SCOPE_FILE).addConnectionCallbacks(this).setAccountName(account).addOnConnectionFailedListener(this).build();
					mGoogleApiClient.connect();
				}
			}
		}
	}

	/**
	 * Called when {@code mGoogleApiClient} is disconnected.
	 */
	@Override
	public void onConnectionSuspended(int cause)
	{
		Log.i(TAG, "GoogleApiClient connection suspended");
		showMessage("Connecting to Drive suspended!");
	}

	/**
	 * Called when {@code mGoogleApiClient} is trying to connect but failed.
	 * Handle {@code result.getResolution()} if there is a resolution is
	 * available.
	 */
	@Override
	public void onConnectionFailed(ConnectionResult result)
	{
		Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
		if (!result.hasResolution())
		{
			// show the localized error dialog.
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
			return;
		}
		try
		{
			result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
		} catch (SendIntentException e)
		{
			Log.e(TAG, "Exception while starting resolution activity", e);
			showMessage("Connecting to Drive failed!");
		}
	}

	/**
	 * Called when {@code mGoogleApiClient} is connected.
	 */
	@Override
	public void onConnected(Bundle connectionHint)
	{
		Log.i(TAG, "GoogleApiClient connected");
		showMessage("Connected to Drive!");

		Log.i(TAG, "Started Drive folder listing.");
		checkMainFolder();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		if (mResults != null)
			mResults.clear();
	}

	/**
	 * Shows a toast message.
	 */
	public void showMessage(String message)
	{
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	/**
	 * Getter for the {@code GoogleApiClient}.
	 */
	public GoogleApiClient getGoogleApiClient()
	{
		return mGoogleApiClient;
	}

	private void connectDrive()
	{
		if (mGoogleApiClient == null)
		{
			mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Drive.API).addScope(Drive.SCOPE_FILE).addScope(Drive.SCOPE_APPFOLDER).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
		}
		mGoogleApiClient.connect();
	}

	protected void connectDriveWithNewAccount()
	{
		Intent accountChooser = AccountPicker.newChooseAccountIntent(null, null, null, true, null, null, null, null);

		startActivityForResult(accountChooser, DRIVE_ACCOUNT_RESELECT);

		// if(driveClient != null)
		// {
		// driveClient.stopAutoManage();
		// driveClient = null;
		// }
	}

	protected boolean IsMainFolderCreated()
	{
		for (Metadata meta : mResults)
		{
			if (meta.getTitle().equalsIgnoreCase("MaNoteApp") && meta.isFolder() && meta.isTrashed() == false)
			{
				Log.i(TAG, "Main folder already exists!");
				mMainFolderId = meta.getDriveId();
				return true;
			}
		}
		Log.i(TAG, "Main folder does not exist!");
		return false;
	}

	/**
	 * Retrieves results for the next page. For the first run, it retrieves
	 * results for the first page.
	 */
	private void checkMainFolder()
	{
		Log.i(TAG, "Next page");
		// if there are no more results to retrieve,
		// return silently.
		if (mHasMore == false)
		{
			return;
		}

		DriveFolder folder = Drive.DriveApi.getRootFolder(getGoogleApiClient());
		folder.listChildren(mGoogleApiClient).setResultCallback(new ResultCallback<MetadataBufferResult>()
		{
			@Override
			public void onResult(MetadataBufferResult result)
			{
				if (!result.getStatus().isSuccess())
				{
					showMessage("Problem while retrieving files");
					return;
				}

//				showMessage("Elkezdtem, e!");
				for (Metadata meta : result.getMetadataBuffer())
				{
					if (meta.isTrashed() == false && meta.isFolder() == true)
					{
						Log.i(TAG, "Added to list: " + meta.getTitle() + ", id=" + meta.getDriveId());
						mResults.add(meta);
					}
				}
				mNextPageToken = result.getMetadataBuffer().getNextPageToken();
				mHasMore = mNextPageToken != null;

				if (IsMainFolderCreated())
					return;

				MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle("MaNoteApp").build();
				Drive.DriveApi.getRootFolder(getGoogleApiClient()).createFolder(getGoogleApiClient(), changeSet).setResultCallback(new ResultCallback<DriveFolderResult>()
				{
					@Override
					public void onResult(DriveFolderResult result)
					{
						if (!result.getStatus().isSuccess())
						{
							showMessage("Error while trying to create MaNoteApp folder");
							return;
						}
						Log.i(TAG, "Folder was created.");
						showMessage("Created MaNoteApp folder: " + result.getDriveFolder().getDriveId());
						mMainFolderId = result.getDriveFolder().getDriveId();
					}
				});
			}
		});
	}

//	/**
//	 * Must not run on UI thread
//	 * @param files
//	 * @return
//	 */
//	protected boolean isFileCreated(File file, MetadataBufferResult result)
//	{
//		final boolean isCreated = false;
//		String fileName = file.getName();
//		MetadataBuffer buffer = result.getMetadataBuffer();
//		
//		for (Metadata meta : result.getMetadataBuffer())
//		
//		//DriveFolder folder = Drive.DriveApi.getFolder(mGoogleApiClient, mMainFolderId);
//		//folder.listChildren(mGoogleApiClient).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>()
//		//{
//			//@Override
//			//public void onResult(MetadataBufferResult result)
////			{
////				for (Metadata meta : result.getMetadataBuffer())
////				{
////					for(File f : files)
////						if(meta.getTitle().equals(f.getName()))
////							isCreated = true;
////				}
////			}
////		});
//		return isCreated;
//	}
	
	protected void syncFile(final NoteFront noteFront)
	{
		if(noteFront.getDriveId() != null && noteFront.getDriveId().isEmpty() == false)
		{
			DriveId id = DriveId.decodeFromString(noteFront.getDriveId());
			final DriveFile dFile = Drive.DriveApi.getFile(mGoogleApiClient, id);
			
			dFile.getMetadata(mGoogleApiClient).setResultCallback(new ResultCallback<DriveResource.MetadataResult>()
			{
				@Override
				public void onResult(MetadataResult result)
				{
					if(result.getStatus().isSuccess())
					{
						Date modifiedDrive = result.getMetadata().getModifiedDate();
						final File file = new File(noteFront.getFileDir());
						Date modifiedLocal = new Date(file.lastModified());
						
						if(modifiedDrive.before(modifiedLocal))
						{
							new EditContentsAsyncTask(DriveActivity.this).execute(dFile,noteFront);
						}
						if(modifiedDrive.after(modifiedLocal))
						{
							dFile.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>()
							{
								@Override
								public void onResult(DriveContentsResult result)
								{
									if(result.getStatus().isSuccess())
									{
										DriveContents contents = result.getDriveContents();
										try
										{
											FileUtils.copyInputStreamToFile(contents.getInputStream(), file);
										} catch (IOException e)
										{
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								}
							});
							
						}
					}
					
				}
			});
		}
	}

	protected void uploadNewFileToDrive(final NoteFront noteFront, final NoteDbLoader dbLoader)
	{
		Drive.DriveApi.newContents(getGoogleApiClient()).setResultCallback(new ResultCallback<DriveApi.ContentsResult>()
		{
			@Override
			public void onResult(ContentsResult result)
			{
				if (!result.getStatus().isSuccess())
				{
					showMessage("Error while trying to create new file contents");
					return;
				}
				DriveFolder folder = Drive.DriveApi.getFolder(getGoogleApiClient(), mMainFolderId);

				MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(new File(noteFront.getFileDir()).getName()).setMimeType("text/mano").build();
				folder.createFile(getGoogleApiClient(), changeSet, result.getContents()).setResultCallback(new ResultCallback<DriveFileResult>()
				{
					@Override
					public void onResult(DriveFileResult result)
					{
						if (!result.getStatus().isSuccess())
						{
							showMessage("Error while trying to create the file");
							return;
						}
						showMessage("Created a file: " + result.getDriveFile().getDriveId());

						DriveFile dFile = result.getDriveFile();
						noteFront.setDriveId(dFile.getDriveId().encodeToString());
						dbLoader.updateNote(noteFront.getId(), noteFront);
						new EditContentsAsyncTask(DriveActivity.this).execute(dFile, noteFront);
					}
				});
			}
		});
	}

	public class EditContentsAsyncTask extends ApiClientAsyncTask<Object, Void, Boolean>
	{

		public EditContentsAsyncTask(Context context)
		{
			super(context);
		}

		@Override
		protected Boolean doInBackgroundConnected(Object... args)
		{
			DriveFile dFile = (DriveFile) args[0];
			NoteFront noteFront = (NoteFront) args[1];
			File file = new File(noteFront.getFileDir());
			try
			{
				ContentsResult contentsResult = dFile.openContents(getGoogleApiClient(), DriveFile.MODE_WRITE_ONLY, null).await();
				if (!contentsResult.getStatus().isSuccess())
				{
					return false;
				}
				byte[] buffer = FileUtils.readFileToByteArray(file);
				OutputStream outputStream = contentsResult.getContents().getOutputStream();
				outputStream.write(buffer);
				com.google.android.gms.common.api.Status status = dFile.commitAndCloseContents(getGoogleApiClient(), contentsResult.getContents()).await();
				if(status.getStatus().isSuccess())
				{
					return true;
				}
				return false;
			} catch (IOException e)
			{
				Log.e(TAG, "IOException while appending to the output stream", e);
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			if (!result)
			{
				showMessage("Error while editing contents");
				return;
			}
			showMessage("Successfully edited contents");
		}
	}
}
