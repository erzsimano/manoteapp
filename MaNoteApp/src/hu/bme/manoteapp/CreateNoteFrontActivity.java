package hu.bme.manoteapp;

import hu.bme.manoteapp.NoteFront.IconType;
import hu.bme.manoteapp.database.NoteDbHelper;
import hu.bme.manoteapp.database.NoteDbLoader;
import hu.bme.manoteapp.database.NoteTableConstants;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class CreateNoteFrontActivity extends Activity
{
	private ArrayList<NoteFront> array;
	private EditText e_text;
	private Button btn_ok;
	private Button btn_cancel;
	private Spinner spinner;
	private ArrayList<String> icontype;
	NoteDbLoader dbLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_note_front);
		setTitle("Create a new note");
		e_text = (EditText) findViewById(R.id.createdialog_et);
		spinner = (Spinner) findViewById(R.id.spinner);
		btn_ok = (Button) findViewById(R.id.createdialog_btn_ok);
		btn_cancel = (Button) findViewById(R.id.createdialog_btn_cancel);
		icontype = new ArrayList<String>();
		dbLoader = new NoteDbLoader(getApplicationContext());

		addItemSpinner();

		btn_ok.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				boolean isTitleValid = true;
				
				Cursor c = dbLoader.fetchAll();
				if(c.moveToFirst())
				{
					while(c.isAfterLast() == false)
					{
						String title = c.getString(c.getColumnIndex(NoteTableConstants.NoteFront.COLUMN_TITLE));
						if(title.equals(e_text.getText().toString()))
						{
							e_text.setError("This name is already taken!");
							isTitleValid = false;
							c.close();
							break;
						}
						c.moveToNext();
					}
				}
				if(e_text.getText().toString().length() == 0)
				{
					e_text.setError("You have to give a title to the note!");
				} else if(isTitleValid)
				{
					NoteFront.IconType icon;
					switch (spinner.getSelectedItemPosition())
					{
						case 0:
							icon = IconType.yellow;
							break;
						case 1:
							icon = IconType.red;
							break;
						case 2:
							icon = IconType.white;
							break;
						case 3:
							icon = IconType.green;
							break;
						case 4:
							icon = IconType.blue;
							break;
						case 5:
							icon = IconType.grey;
							break;
						case 6:
							icon = IconType.purple;
							break;
						default:
							icon = IconType.yellow;
					}

					Intent retIntent = new Intent();
					long genId = Math.abs(new Random().nextLong());
					
					String title = e_text.getText().toString();
					String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + MainActivity.FILE_DIR
							+ File.separator + title + ".mano";
					retIntent.putExtra("hu.bme.manoteapp.NoteFront", new NoteFront(genId, title, spinner.getSelectedItemPosition(), dir, null));

					setResult(100, retIntent);
					finish();
				}
			}
		});

		btn_cancel.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
		
		dbLoader.open();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		dbLoader.close();
	}
	
	public void addItemSpinner()
	{
		icontype.add("Yellow");
		icontype.add("Red");
		icontype.add("White");
		icontype.add("Green");
		icontype.add("Blue");
		icontype.add("Grey");
		icontype.add("Purple");
		// SpinnerAdapter adapter = new SpinnerAdapter(icontype);
		ArrayAdapter<String> arr = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, icontype);
		arr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(arr);
	}

}
