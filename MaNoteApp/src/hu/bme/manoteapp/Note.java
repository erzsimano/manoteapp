package hu.bme.manoteapp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable
{
	private SmartPath path;
	private int textColor;
	private StringBuffer text;

	public Note()
	{
		path = new SmartPath();
		textColor = 0;
		text = new StringBuffer();
	}

	public Note(SmartPath path, String text)
	{
		this.path = path;
		textColor = 0;
		this.text = new StringBuffer(text);
	}

	public Note(File file) throws ClassNotFoundException, IOException
	{
		this.path = new SmartPath();
		this.text = new StringBuffer();

		if (file.exists() == false || file.length() == 0)
		{
			textColor = Color.BLACK;
		} else
		{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			textColor = (Integer) ois.readObject();
			text.append((String) ois.readObject());
			path.deSerialize(ois);

			ois.close();
		}
	}

	public Note(Parcel in)
	{
		path = new SmartPath();
		path.setCurves(in.readArrayList(Curve.class.getClassLoader()));
		textColor = in.readInt();
		text.setLength(0);
		text.append(in.readString());
	}

	public void save(File file) throws FileNotFoundException, IOException
	{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));

		oos.writeObject(textColor);
		oos.writeObject(text.toString());
		path.serialize(oos);

		oos.close();
	}

	public void setText(String text)
	{
		this.text.setLength(0);
		this.text.append(text);
	}

	public void addCurve(Curve curve)
	{
		path.addCurve(curve);
	}

	public SmartPath getDrawablePath()
	{
		return path;
	}

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags)
	{
		parcel.writeList(path.getCurves());
		parcel.writeInt(textColor);
		parcel.writeString(text.toString());
	}

	public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>()
	{
		public Note createFromParcel(Parcel in)
		{
			return new Note(in);
		}

		public Note[] newArray(int size)
		{
			return new Note[size];
		}
	};
}
