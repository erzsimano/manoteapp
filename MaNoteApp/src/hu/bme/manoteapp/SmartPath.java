package hu.bme.manoteapp;

import hu.bme.manoteapp.Curve.SerializableColor;
import hu.bme.manoteapp.Curve.SerializablePoint;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import com.itextpdf.awt.geom.Point2D;

public class SmartPath
{
	private List<Curve> curves;
	private Path drawerPath;

	public SmartPath()
	{
		curves = new ArrayList<Curve>();
		drawerPath = new Path();
	}

	public void draw(Canvas canvas, Paint paint)
	{
		double cx, cy;

		for(Curve curve : curves)
		{
			drawerPath.reset();
			List<Curve.SerializablePoint> points = curve.getPoints();
			cx = points.get(0).getX();
			cy = points.get(0).getY();
			drawerPath.moveTo((float) cx, (float) cy);
			for(int i = 0; i < points.size(); i++)
			{
				SerializablePoint p = points.get(i);
				Point2D.Double point = new Point2D.Double(p.getX(), p.getY());
				cx = point.x;
				cy = point.y;
				drawerPath.quadTo((float) cx, (float) cy, (float) ((cx + point.x) / 2.0f), (float) ((cy + point.y) / 2.0f));
			}

			SerializableColor c = curve.getColor();
			paint.setColor(Color.rgb(c.r, c.g, c.b));
			canvas.drawPath(drawerPath, paint);
		}
	}

	// G�rbe hozz�ad�sa a path-hoz
	// EZZEL tudod �p�tgetni a path-t, a kattint�s-h�z�s-elenged�s m�veletsorn�l
	// egy Curve-t kell �p�tened, amit majd ide �tadsz
	public void addCurve(Curve curve)
	{
		curves.add(curve);
	}

	public void serialize(ObjectOutputStream oos) throws IOException
	{
		oos.writeObject(curves);
	}

	@SuppressWarnings("unchecked")
	public void deSerialize(ObjectInputStream ois) throws ClassNotFoundException, IOException
	{
		curves.clear();

		curves = (List<Curve>) ois.readObject();
	}

	public void reset()
	{
		curves.clear();
		drawerPath.reset();
	}

	// Majd ha ki akarod menteni a pontokat, j� lesz elk�rni
	public List<Curve> getCurves()
	{
		return curves;
	}

	public void setCurves(List<Curve> curves)
	{
		this.curves = curves;
	}

}
