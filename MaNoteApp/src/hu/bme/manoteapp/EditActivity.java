package hu.bme.manoteapp;

import hu.bme.manoteapp.DrawView.OnDrawChanged;
import hu.bme.manoteapp.color.ColorPicker;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;

public class EditActivity extends Activity implements ColorPicker.OnColorChangedListener, TextWatcher, OnDrawChanged
{
	private EditText edittext;
	private String title;
	public static final String AUTHOR = "Beke_Erzs�bet";
	private boolean isDrawView = false;
	private DrawView dView;
	private int textColor = 0;
	private boolean isDirty = false;
	private boolean isClear = false;
	private Note note;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_edit);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		Intent intent = getIntent();

		dView = (DrawView) findViewById(R.id.draw_view);
		edittext = (EditText) findViewById(R.id.edittext);

		if (savedInstanceState != null)
			title = savedInstanceState.getString("NOTE_TITLE");
		else
			title = intent.getStringExtra("note_title");

		setTitle(title);

		// readFromFile(title);

		dView.setFocusable(false);
		dView.setDrawingCacheEnabled(true);
		edittext.setDrawingCacheEnabled(true);

		loadNote(title);

		edittext.addTextChangedListener(this);
		dView.addDrawChangedListener(this);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	public void swapViews()
	{
		if (isDrawView == false && isClear == false)
		{
			edittext.setFocusable(false);

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);

			dView.setFocusable(true);
			dView.bringToFront();
			dView.requestFocus();
			isDrawView = true;

		} else if (isDrawView == true && isClear == false)
		{
			dView.setFocusable(false);
			edittext.setFocusableInTouchMode(true);
			edittext.bringToFront();

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(edittext, 0);

			isDrawView = false;
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		try
		{
			if (note != null)
				saveNote();
			outState.putString("NOTE_TITLE", title);
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// outState.putString("Drawview", getViewAsBase64());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.menu_editactivity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case R.id.edit_save:
			try
			{
				saveNote();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case R.id.edit_export:
			exportToPDF();
			break;
		case android.R.id.home:
			if (isDirty == true)
				showAlert();
			else
				NavUtils.navigateUpFromSameTask(this);
			break;
		case R.id.edit_draw:
			swapViews();
			if (isDrawView)
				item.setIcon(android.R.drawable.ic_menu_edit);
			else
			{
				item.setIcon(android.R.drawable.ic_menu_sort_alphabetically);
			}
			break;
		case R.id.edit_color:
			new ColorPicker(this, this, Color.BLACK).show();
			break;
		case R.id.edit_clear:
			dView.clear();
			isClear = !isClear;
			break;

		}
		return true;
	}

	@Override
	public void onBackPressed()
	{
		if (isDirty == true)
			showAlert();
		else
			finish();
	}

	public void showAlert()
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("Exit");
		alertDialogBuilder.setMessage("Exit without saving?");
		alertDialogBuilder.setCancelable(false);

		alertDialogBuilder.setPositiveButton("Yes", new OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				EditActivity.this.finish();
			}
		});

		alertDialogBuilder.setNegativeButton("No", new OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.cancel();
			}
		});

		AlertDialog alertdialog = alertDialogBuilder.create();
		alertdialog.show();
	}

	@Deprecated
	public void exportToPDF()
	{
		Document doc = new Document(PageSize.A4);
		Bitmap b = dView.getDrawingCache();
		Image image = null;
		File output = null;

		Locale local = getResources().getConfiguration().locale;

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		b.compress(Bitmap.CompressFormat.PNG, 85, bout);

		try
		{
			image = Image.getInstance(bout.toByteArray());
			String dir = Environment.getExternalStorageDirectory() + File.separator + MainActivity.FILE_DIR + File.separator + title + ".pdf";
			output = new File(dir);
			if (output.getParentFile().exists() == false)
				output.getParentFile().mkdir();
			if (output.exists() == false)
				output.createNewFile();

			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(output));
			pdfWriter.setLanguage(local.getLanguage());
			Font font = FontFactory.getFont(getResources().openRawResource(R.raw.times).toString(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 10.0f, 10, new BaseColor(textColor));

			doc.open();
			doc.addTitle(title);
			doc.addCreationDate();
			doc.addAuthor(AUTHOR);
			doc.addSubject("MaNote export");
			doc.setPageCount(1);
			doc.addLanguage(local.getLanguage());
			doc.add(new Paragraph(edittext.getText().toString(), font));
			doc.add(image);

			Toast.makeText(getApplicationContext(), "Pdf file created: " + dir, Toast.LENGTH_LONG).show();

			pdfWriter.setPdfVersion(PdfWriter.PDF_VERSION_1_4);
			doc.close();

		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
			Log.d("EDIT", e.getMessage());
		} catch (DocumentException e)
		{
			e.printStackTrace();
			Log.d("EDIT", e.getMessage());
		} catch (IOException e)
		{
			e.printStackTrace();
			Log.d("EDIT", e.getMessage());
		}
	}

	@Deprecated
	public String getViewAsBase64()
	{
		Bitmap save = dView.getDrawingCache();
		String base64Image;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		save.compress(Bitmap.CompressFormat.PNG, 100, baos);
		byte[] b = baos.toByteArray();

		base64Image = Base64.encodeBytes(b);

		return base64Image;
	}

	@Deprecated
	public Bitmap getBitmapFromBase64(String b64)
	{
		byte[] b = Base64.decode(b64);

		return BitmapFactory.decodeByteArray(b, 0, b.length);
	}

	public void saveNote() throws IOException
	{
		String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + MainActivity.FILE_DIR;
		File output = new File(dir);
		if (output.exists() == false)
			output.mkdir();
		output = new File(dir + File.separator + title + ".mano");
		if (output.exists() == false)
			output.createNewFile();

		new NoteSaverTask().execute(output.getAbsolutePath());
	}

	public void loadNote(String title)
	{
		String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + MainActivity.FILE_DIR;
		String fullDir = dir + File.separator + title + ".mano";
		new NoteLoaderTask().execute(fullDir);
	}

	@Override
	public void colorChanged(int color)
	{
		if (isDrawView == true)
			dView.setColorPaint(color);
		else
		{
			textColor = color;
			edittext.setTextColor(textColor);
		}
	}

	@Override
	public void afterTextChanged(Editable s)
	{
//		isDirty = true;
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
		// isModifiedText = true;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		isDirty = true;
	}

	@Override
	public void DrawChanged()
	{
		isDirty = true;
	}

	class NoteSaverTask extends AsyncTask<String, Integer, Boolean>
	{
		@Override
		protected Boolean doInBackground(String... params)
		{
			try
			{
				note.save(new File(params[0]));
			} catch (FileNotFoundException e)
			{
				e.printStackTrace();
				return false;
			} catch (IOException e)
			{
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			final String resultIndicator;
			if (result)
			{
				resultIndicator = "Note was saved successfully";
				isDirty = false;
			}
			else
				resultIndicator = "Note cannot be saved";

			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					Toast.makeText(getApplicationContext(), resultIndicator, Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	class NoteLoaderTask extends AsyncTask<String, Integer, Boolean>
	{
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute()
		{
			pDialog = ProgressDialog.show(EditActivity.this, null, "Loading note...");
			pDialog.setCancelable(false);
		}

		@Override
		protected Boolean doInBackground(String... params)
		{
			try
			{
				note = new Note(new File(params[0]));
				dView.setNote(note);
			} catch (ClassNotFoundException e)
			{
				e.printStackTrace();
				return false;
			} catch (IOException e)
			{
				e.printStackTrace();
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			pDialog.dismiss();
			dView.invalidate();
			if (result)
			{
			} else
			{
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						Toast.makeText(getApplicationContext(), "Could not load note!", Toast.LENGTH_SHORT).show();
						finish();
					}
				});
			}

		}

	}
}