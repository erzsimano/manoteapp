package hu.bme.manoteapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View
{
	private Canvas mCanvas;
	private Paint mPaint;
	private OnDrawChanged listener;
	private Curve mCurve;

	private Rect camRect;

	private Note note;
	
	private PointF motionTracker;

	public static interface OnDrawChanged
	{
		public void DrawChanged();
	}

	public DrawView(Context c)
	{
		super(c);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
		setColorPaint(0xFF000000);
		motionTracker = new PointF();
		camRect = new Rect();
	}

	public DrawView(Context c, AttributeSet attrs)
	{
		super(c, attrs);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(0xFF000000);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(3);
		motionTracker = new PointF();
		camRect = new Rect();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
		camRect.left = 0;
		camRect.top = 0;
		camRect.right = w;
		camRect.bottom = h;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		canvas.translate(camRect.left, camRect.top);
		if (note != null)
			note.getDrawablePath().draw(canvas, mPaint);
	}

	private float mX, mY;
	private static final float TOUCH_TOLERANCE = 4;

	private void touch_start(float x, float y)
	{
		mCurve = new Curve();
		mCurve.setColor(mPaint.getColor());
		mCurve.addPoint(camRect.left + x, camRect.top + y);
		note.addCurve(mCurve);
		invalidate();
	}

	private void touch_move(float x, float y)
	{
		mCurve.addPoint(camRect.left + x, camRect.top + y);
		invalidate();
	}

	private void touch_up()
	{
		invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (event.getPointerCount() == 1)
		{
			float x = event.getX();
			float y = event.getY();
			if (listener != null)
				listener.DrawChanged();

			switch (event.getAction())
			{
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				break;
			}
			return true;
		}
		else if(event.getPointerCount() == 2)
		{
			switch (event.getAction())
			{
			case MotionEvent.ACTION_DOWN:
				motionTracker.x = event.getX(1);
				motionTracker.y = event.getY(1);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				int diffX = (int) (motionTracker.x - event.getX(1));
				int diffY = (int) (motionTracker.y - event.getY(1));
				if(event.getX(1) < motionTracker.x)
					moveCameraBy(diffX, 0);
				else
					moveCameraBy(-diffX, 0);
				if(event.getY(1) < motionTracker.y)
					moveCameraBy(0, diffY);
				else
					moveCameraBy(0, -diffY);
				invalidate();
				break;
			}
			return true;
		}
		
		return false;
	}

	public void clear()
	{
		int w = this.getWidth();
		int h = this.getHeight();
		Paint paint = new Paint();
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.TRANSPARENT);
		mCanvas.drawCircle(w / 2, h / 2, 10, paint);

		paint.setStyle(Paint.Style.STROKE);
		paint.setColor(Color.BLACK);
		mCanvas.drawCircle(w / 2, h / 2, 10, paint);

		// paint.setAntiAlias(true);
		// paint.setXfermode(new
		// PorterDuffXfermode(android.graphics.PorterDuff.Mode.CLEAR));

		// mBitmap.eraseColor(Color.TRANSPARENT);

		// mCanvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
		invalidate();
	}

	public void setColorPaint(int color)
	{
		mPaint.setColor(color);
	}

	public int getColorPaint()
	{
		return mPaint.getColor();
	}

	public void addDrawChangedListener(OnDrawChanged o)
	{
		listener = o;
	}

	public void setNote(Note note)
	{
		this.note = note;
	}

	public void setCamera(int x, int y)
	{
		camRect.left = x;
		camRect.top = y;
	}

	public void moveCameraBy(int x, int y)
	{
		camRect.left += x;
		camRect.top += y;
	}
}
